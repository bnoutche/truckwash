﻿using System;
using System.Collections.Generic;
using System.Linq;
using TruckWash.Models;
using System.Data.Entity;

namespace TruckWash.BLL
{
    public class WashTurnBl
    {
        #region Database
        private TruckwashContext _db = new TruckwashContext();
        #endregion

        #region Methods
        //public List<WashTurn> GetAllWashTurns()
        //{

        //    return _db.WashTurn.Include(w => w.Vehicle.Customer).Include(w => w.Vehicle).Include(w => w.WashType).Include(w => w.WashTypeExtra).ToList();
        //}

        public List<WashTurn> GetAllPlannedWashTurns()
        {
   
            return _db.WashTurn.Where(w => w.EndTime == null && w.StartTime == null).Include(w => w.Vehicle.Customer).Include(w => w.Vehicle).Include(w => w.WashType).Include(w => w.WashTypeExtra).ToList();
        }

        public List<WashTurn> GetAllStartedWashTurns()
        {

            return _db.WashTurn.Where(w => w.EndTime == null && w.StartTime != null).Include(w => w.Vehicle.Customer).Include(w => w.Vehicle).Include(w => w.WashType).Include(w => w.WashTypeExtra).ToList();
        }

        public WashTurn GetWashTurn(int? id)
        {
          
            return _db.WashTurn.Find(id);
        }

        public WashTurn CreateWashTurn(WashTurn washTurn)
        {
            washTurn.WaitTime = DateTime.Now;
            washTurn.WashType = _db.WashType.Find(washTurn.WashTypeID);
            washTurn.WashTypeExtra = _db.WashTypeExtra.Find(washTurn.WashTypeExtraID);
            washTurn.Vehicle = _db.Vehicle.Find(washTurn.VehicleID);
            _db.WashTurn.Add(washTurn);
            _db.SaveChanges();
            return washTurn;
        }

        public WashTurn CreateWashTurnNewCustomer(WashTurn washTurn)
        {


            washTurn.Vehicle.Customer.IsActive = true;
            washTurn.WaitTime = DateTime.Now;
            _db.WashTurn.Add(washTurn);
            _db.SaveChanges();
            return washTurn;
        }

        public WashTurn UpdateWashTurn(WashTurn washTurn)
        {
            _db.Entry(washTurn).State = EntityState.Modified;
            _db.SaveChanges();
            return washTurn;
        }

        public void DeleteWashTurn(int id)
        {

            WashTurn washTurn=GetWashTurn(id);
            _db.WashTurn.Remove(washTurn);
            _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
        }
#endregion
    }
}