﻿using System.Collections.Generic;
using System.Linq;
using TruckWash.Models;
using System.Data.Entity;
namespace TruckWash.BLL
{
    public class LoginLogBL
    {
        #region Database
        private TruckwashContext db = new TruckwashContext();
        #endregion

        #region Methods
        public List<LoginLog> GetAllLoginlogs()
        {
            return db.LoginLog.ToList();
        }

        public LoginLog GetLoginlogById(int id)
        {
            return db.LoginLog.Find(id);
        }

        public List<LoginLog> GetAllActiveLoginlogs()
        {
            return db.LoginLog.Where(s => s.Staff.IsActive == true).ToList();
        }

        public void SaveLoginlog(LoginLog loginLog)
        {
            db.LoginLog.Add(loginLog);
            db.SaveChanges();
        }

        public void UpdateLoginlog(LoginLog loginLog)
        {
            db.Entry(loginLog).State = EntityState.Modified;
            db.SaveChanges();
        }

        public List<LoginLog> GetAllDeactivatedLoginlogs()
        {
            return db.LoginLog.Where(s => s.Staff.IsActive == false).ToList();
        }

        public void DeleteLoginlog(int id)
        {
            LoginLog log = GetLoginlogById(id);
            db.LoginLog.Remove(log);
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
        #endregion
    }
}