﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using TruckWash.Models;

namespace TruckWash.BLL
{
    public class StaffBl
    {

        #region Database
        private TruckwashContext _db = new TruckwashContext();
        #endregion 
        
        #region BLL
        private QrCodeBl _qrCodeBl = new QrCodeBl();
        #endregion 
        
        #region Models
        private QrCodes _qrcode = new QrCodes();
        #endregion 

        #region Private strings
        private string _user = "Operator";
        private string _admin = "Administrator";        
        #endregion

        #region methods
        public List<Staff> GetAllStaffs()
        {
            return _db.Staff.ToList();
        }

      
        public List<Staff> GetAllActiveStaffsWithLocation()
        {
            return _db.Staff.Where(s => s.IsActive == true).ToList();
        }

        public List<Staff> GetAllDeactiveStaffsWithLocation()
        {
            return _db.Staff.Where(s => s.IsActive == false).ToList(); ;
        }

        public Staff GetStaffById (int? id)
        {
            return _db.Staff.Find(id);
        }

        public List<string> GetJobtitels()
        {
            List<string> list = new List<string>();
            list.Add(_user);
            list.Add(_admin);
            return list;
        }

        public void CreateNewStaffMemberWithQrCode (Staff Staff)
        {
            //Nieuw staff is altijd actief :-)
            Staff.IsActive = true;
      //De qrcode - zal altijd dezelfde blijven : bestaande uit een voorvoegsel (username = ) en de usernam - deze is altijd uniek :-)         
            _db.Staff.Add(Staff);
            _db.SaveChanges();

            //Toevoegen van een qrcode
            _qrcode = new QrCodes();
            _qrcode = _qrCodeBl.GenerateQrCodeFromCreateStaff(Staff);

            //Het passwoord is std de staff first name en de stafnumber aan elkaar -> dit kan dan door de staffmember zelf worden gewijzigd
            _qrcode.Password = (Staff.FirstName + Staff.StafNumber.ToString()).ToUpper();
            _db.QrCodes.Add(_qrcode);
            _db.SaveChanges();
        }

        public void EditStaffMember(Staff editetStaffDetails)
        {
            _db.Entry(editetStaffDetails).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public QrCodes GetQrCodeFromStaffId(int id)
        {
            _qrcode = new QrCodes();
            // nog omzetten in een db.where clause
            foreach (QrCodes selectedQrCode in _db.QrCodes)
            {
                if (selectedQrCode.StaffID == id)
                {
                    _qrcode = selectedQrCode;
                    return selectedQrCode;
                }
            } 
            return _qrcode;
        }
        
        public void ActivateAndDeactivateStaffMember(Staff activateAndDeactivate)
        {
            if (activateAndDeactivate.IsActive == true)
            {
                activateAndDeactivate.IsActive = false;
            }
            else
            {
                activateAndDeactivate.IsActive = true;
            }        
            EditStaffMember(activateAndDeactivate);    
        }

        public void Dispose()
        {
            _db.Dispose();
        }
        #endregion

    }
}