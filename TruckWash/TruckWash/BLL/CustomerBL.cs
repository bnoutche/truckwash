﻿
using System.Collections.Generic;
using System.Linq;
using TruckWash.Models;
using System.Data.Entity;


namespace TruckWash.BLL
{

    public class CustomerBl
    {
        #region Database
        private TruckwashContext _db = new TruckwashContext();
        #endregion

        #region BLL
        private AddressBl _adress = new AddressBl();
        #endregion

        #region Methods

        /// <summary>
        /// Create Customer 
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>New Customer</returns>
        public Customer CreateCustomer(Customer customer)
        {
            customer.IsActive = true;
            _db.Customer.Add(customer);
            _db.SaveChanges();

            return customer;
        }

        /// <summary>
        /// Get Customer List 
        /// </summary>
        /// <returns>List of Customers</returns>
        public List<Customer> GetAllCustomer()
        {

            return _db.Customer.Include(c => c.Address).ToList();
        }

        public List<Customer> GetAllActiveCustomers()
        {
            return _db.Customer.Where(c => c.IsActive == true).Include(c => c.Address).ToList();
        }

        public List<Customer> GetAllNotActiveCustomers()
        {

            return _db.Customer.Where(c => c.IsActive == false).Include(c => c.Address).ToList();
        }

        /// <summary>
        /// Get one Customer With details
        /// </summary>
        /// <param name="id"></param>
        /// <returns>One customer With All details</returns>
        public Customer GetCustomer(int? id)
        {
            return _db.Customer.Find(id);
        }

        /// <summary>
        /// Update a customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>upated Customer</returns>
        public Customer UpdateCustomer(Customer customer)
        {
            //customer.Address.IdAddress = customer.AddressID.Value;
            _db.Entry(customer).State = EntityState.Modified;
            _db.SaveChanges();
            return customer;
        }
        
        /// <summary>
        /// Activate of deactivate customer
        /// </summary>
        /// <param name="isTrue"></param>
        /// <param name="id"></param>
        /// <returns>state change customer</returns>
        public void IsAcive(bool isTrue, int id)
        {
            Customer changeActiveState = GetCustomer(id);
            if (isTrue)
            {

                if (changeActiveState != null)
                {
                    if (changeActiveState.IsActive != true)
                    {
                        changeActiveState.IsActive = true;

                    }

                    else
                    {
                        changeActiveState.IsActive = false;

                    }
                    UpdateCustomer(changeActiveState);

                }

            }

        }

        /// <summary>
        /// Get all vehicle own by customer
        /// </summary>
        /// <param name="id"></param>
        /// <returns>vehicle from customer </returns>
        public List<Vehicle> GetVehicle(int id)
        {
            return _db.Vehicle.Where(model => model.CustomerID == id).ToList();
            
        }
        
        /// <summary>
        /// Get all invoices own by customer
        /// </summary>
        /// <param name="id"></param>
        /// <returns>invoices from customer</returns>
        public List<Invoice> GetInvoices(int? id)
        {
            return _db.Invoice.Where(i => i.WashTurn.Vehicle.CustomerID == id).ToList();
        }
        
        public void Dispose()
        {
            _db.Dispose();
        }
        #endregion

    }



}