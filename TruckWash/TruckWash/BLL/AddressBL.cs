﻿using System.Collections.Generic;
using System.Linq;
using TruckWash.Models;
using System.Data.Entity;

namespace TruckWash.BLL
{

    public class AddressBl
    {
        #region Database
        private TruckwashContext _db = new TruckwashContext();
        #endregion

        #region Methods
        public Address CreateAddress(Address address)
        {
            _db.Address.Add(address);
            _db.SaveChanges();
            return address;
        }

        public Address GetAddress(int id)
        {
            Address address = _db.Address.Find(id);
            return address;
        }

        public Address UpdateAddress(Address address)
        {          

            _db.Entry(address).State = EntityState.Modified;
            _db.SaveChanges();

            return address;
        }

        public List<Address> GetAllAddresses()
        {
            return _db.Address.ToList();
        }
        #endregion
    }
}