﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TruckWash.Models;

namespace TruckWash.BLL
{
    public class LocationBL
    {
        #region Database
        private TruckwashContext db = new TruckwashContext();
        #endregion

        #region Methods
        public List<Location> GetAllLocations()
        {
            return db.Location.ToList();
        }

        public Location GetLocationById(int id)
        {
            return db.Location.Find(id); ;
        }

        public void SaveLocation(Location location)
        {
            db.Location.Add(location);
            db.SaveChanges();
        }
        
        public void UpdateLocation(Location location)
        {
            db.Entry(location).State = EntityState.Modified;
            db.SaveChanges();
        }
        
        public void Dispose()
        {
            db.Dispose();
        }
        #endregion
    }
}