﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using TruckWash.Models;

namespace TruckWash.BLL
{
    public class WashTypeBl
    {
        #region Database
        private TruckwashContext _db = new TruckwashContext();
        #endregion

        #region Methods
        public List<WashType> GetAllWashTypes()
        {
          
            return _db.WashType.ToList();

        }

        public WashType GetWashType(int? id)
        {
           
            return _db.WashType.Find(id);
        }

        public WashType CreateWashType(WashType washType)
        {

            _db.WashType.Add(washType);
            _db.SaveChanges();
            return washType;
        }

        public WashType UpdateWashType(WashType washType)
        {
            _db.Entry(washType).State = EntityState.Modified;
            _db.SaveChanges();
            return washType;
        }

        public void DeleteWashType(int id)
        {
            WashType washType = GetWashType(id);
            _db.WashType.Remove(washType);
            _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
        }
#endregion
    }
}