﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using com.google.zxing.common;
using com.google.zxing;
using System.Text;
using System.IO;
using TruckWash.Models;
using System.Data.Entity;

namespace TruckWash.BLL
{
    public class QrCodeBl
    {
        #region Database
        private TruckwashContext _db = new TruckwashContext();
        #endregion

        #region Model
        QrCodes _qrcode = new QrCodes();
        #endregion

        #region Private Strings
        //Deze string is het voorvoegsel van de qr code -> indien je deze wijzigd word 
        //de qr anders gemaakt en anders uitgelezen. Dit is het enige voorvoegsel dat in 
        //de code gebruikt wordt. dit aanpassen is voldoende
        private string voorvoegselQrCode = "vuylsteketransport.wix.com/vuylsteketransport/";
        #endregion

        #region Methods
        public List<QrCodes> GetAllQrCodes()
        {
            return _db.QrCodes.ToList();
        }

        public void UpdateQrCode(QrCodes qrcode)
        {
            _db.Entry(qrcode).State = EntityState.Modified;
            _db.SaveChanges();
        }
                     
        public void Dispose ()
        {
            _db.Dispose();
        }

        public void DeleteQrCode(QrCodes qrCode)
        {
            _db.QrCodes.Remove(qrCode);
            _db.SaveChanges();
        }

        public void SaveQrcCode(QrCodes qrCode)
        {
            _db.QrCodes.Add(qrCode);
            _db.SaveChanges();
        }

        public QrCodes GenerateQrCodeFromCreateStaff(Staff staff)
        {
            _qrcode = new QrCodes();
            _qrcode.StaffID = staff.IdStaff;
            _qrcode.Staff = staff;
                       
            Random r = new Random();

            string staffnumber = staff.StafNumber.ToString();
            char[] stringArray = staffnumber.ToCharArray();
            string encryptedstaffnumber = string.Empty;

            for (int i = 0; i < stringArray.Length; i++)
            {
                //1ste getal = beginstaffnumber
                encryptedstaffnumber += stringArray[i];
                //tussen elk getal van de staffnumber zit een vals nummer - om niet doorzichtig te zijn - anti zelfmaken (adhv de staffnummer)
                encryptedstaffnumber += r.Next(0, 10).ToString();
            }

            string newQrCode = voorvoegselQrCode + encryptedstaffnumber;
            _qrcode.QrCode = EncodeQrCode(newQrCode);
            return _qrcode;
        }

        public QrCodes GetQrCodeById(int id)
        {
            return _db.QrCodes.Find(id);
        }

        public string GetQrPassword(string encryptedPassword)
        {
            char[] arrayFromStringFromScannedPhoto = encryptedPassword.ToCharArray();

            string QrCodeUitgelezenPasswoord = string.Empty;

            for (int i = voorvoegselQrCode.Count(); arrayFromStringFromScannedPhoto.Length > i;)
            {
                QrCodeUitgelezenPasswoord += arrayFromStringFromScannedPhoto[i];
                i += 2;
            }
            return QrCodeUitgelezenPasswoord;
        }

        public string GetScannedQrCodeStringText(string qrCodeFromView)
        {
            string photobase64format = qrCodeFromView;
            byte[] ImageNeedToScan = ConvertBase64ToByteArray(photobase64format);

            Image picture = ByteArrayToImage(ImageNeedToScan);
            Bitmap photoToCheck = (Bitmap)picture;
            return ScanAnImageForQrCode(photoToCheck);
        }

        public List<Bitmap> CreateListTurnedBitmpas(Bitmap bitmap)
        {
            Bitmap zeroDegrees = bitmap;
            Bitmap ninetyDegrees = bitmap;
            Bitmap hundredEighty = bitmap;
            Bitmap twoHundredSeventy = bitmap;
            ninetyDegrees.RotateFlip(RotateFlipType.Rotate90FlipNone);
            hundredEighty.RotateFlip(RotateFlipType.Rotate180FlipNone);
            twoHundredSeventy.RotateFlip(RotateFlipType.Rotate270FlipNone);
            
            List<Bitmap> allPosibleBitmaps = new List<Bitmap>();
            allPosibleBitmaps.Add(zeroDegrees);
            allPosibleBitmaps.Add(ninetyDegrees);
            allPosibleBitmaps.Add(hundredEighty);
            allPosibleBitmaps.Add(twoHundredSeventy);
            return allPosibleBitmaps;
        }

        public string ScanAnImageForQrCode(Bitmap bitmap)
        {
            //Image QrCodeFromScannedPicture = bitmap;
            List<Bitmap> imagesPossibleTurns = CreateListTurnedBitmpas(bitmap);            
            // foto van camera - maw te groot qr staat er ergens midden in                       
            // scannen naar de effectieve qr code
            var reader = new com.google.zxing.qrcode.QRCodeReader();
            string readed = string.Empty;

            foreach (Bitmap item in imagesPossibleTurns)
            {
                if (readed == string.Empty)
                {
                    try
                    {
                        LuminanceSource source = new RGBLuminanceSource(item, item.Width, item.Height);
                        var binarizer = new HybridBinarizer(source);
                        var binBitmap = new BinaryBitmap(binarizer);
                        readed = reader.decode(binBitmap).Text;
                    }
                    catch (Exception)
                    {                //catch the exception and return null instead
                        return null;
                    }
                }
            }
            //            
            if (readed != null) //if the string is NOT null, then we found the QR. If it is null, then the for loop will continue searching if it has more corners to look at
            {
                //database composed text - > firstname + staffnumer
                return readed;
            }
            return null;
        }

        public byte[] ConvertBase64ToByteArray(string base64)
        {
            byte[] newBytes = Convert.FromBase64String(base64);
            return newBytes;
        }

        public void SaveNewPassword (QrCodes qr)
        {
            QrCodes qrcode = GetQrCodeById(qr.IdQrCode);
            qrcode.Password = qr.Password;
        }
        
        public string ConvertByteArrayToString(byte[] bytearrayfromdb)
        {
            StringBuilder hex = new StringBuilder(bytearrayfromdb.Length * 2);
            foreach (byte b in bytearrayfromdb)
            {
                hex.AppendFormat("{0:x2}", b);
            }
            return hex.ToString();
        }
        
        public byte[] EncodeQrCode(string qrcodetext)
        {
            MessagingToolkit.QRCode.Codec.QRCodeEncoder toolkitEncoder = new MessagingToolkit.QRCode.Codec.QRCodeEncoder();
            //create qrcode from text
            Bitmap picture = toolkitEncoder.Encode(qrcodetext);
            ImageConverter imageconverter = new ImageConverter();
            byte[] array = (byte[])imageconverter.ConvertTo(picture, typeof(byte[]));
            return array;
        }

        public Image ByteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
        #endregion
    }
}