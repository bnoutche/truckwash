﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TruckWash.Models;

namespace TruckWash.BLL
{
    public class VehicleBl
    {
        #region Database
        private TruckwashContext _db = new TruckwashContext();
        #endregion

        #region Methods
        public List<Vehicle> GetAllVehicles()
        {
           
            return _db.Vehicle.ToList();
        }

        public List<Vehicle> GetAllVehiclesIntern()
        {

            return _db.Vehicle.Where(v => v.IsInternVehicle == true).Include(v => v.Customer).ToList();
        }
        
        public Vehicle GetVehicleByName(string name)
        {
            return _db.Vehicle.Find(name);
        }

        public Vehicle GetVehicleByPlate(string licenceplate)
        {
          
            return _db.Vehicle.Find(licenceplate);
        }

        public Vehicle GetVehicleById(int? id)
        {

            return _db.Vehicle.Find(id);
        }

        public Vehicle CreateNewVehicle(Vehicle newVehicle)
        {
            _db.Vehicle.Add(newVehicle);
            _db.SaveChanges();
            return newVehicle;
        }

        public void EditVehicle(Vehicle editetVehicleDetails)
        {
            _db.Entry(editetVehicleDetails).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void DeleteVehicle(int? id)
        {
            Vehicle vehicle = GetVehicleById(id);
            _db.Vehicle.Remove(vehicle);
            _db.SaveChanges();
        }
                
        public void Dispose()
        {
            _db.Dispose();
        }
        #endregion
    }
}