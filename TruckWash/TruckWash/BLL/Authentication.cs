﻿using System;
using System.Collections.Generic;
using TruckWash.Models;


namespace TruckWash.BLL
{
    public class Authentication
    {
        #region Database
        private TruckwashContext _db = new TruckwashContext();
        #endregion 

        #region BLL
        private QrCodeBl _qrcode = new QrCodeBl();
        #endregion

        #region Model
        private List<QrCodes> _allAccounts = new List<QrCodes>();
        private LoginLog _log = new LoginLog();
        #endregion

        #region Methods
        //firstname = username       Lastname == paswoord       locationid = locationid
        public LoginLog InloggenFromLoginPage(string username, string password, int idlocation, bool qr)
        {
            _allAccounts = _qrcode.GetAllQrCodes();
            foreach (QrCodes item in _allAccounts)
            {
                if (username.ToUpper() == (item.Staff.FirstName.ToUpper() + item.Staff.StafNumber.ToString()))
                {
                    if (((password.ToUpper() == item.Password.ToUpper())&& qr == false) || qr == true)
                    {
                        _log.Location = _db.Location.Find(idlocation);
                        _log.LocationID = idlocation;
                        _log.Staff = _db.Staff.Find(item.StaffID);
                        _log.Lastworkdate = DateTime.Now;                        
                        _db.LoginLog.Add(_log);
                        _db.SaveChanges();
                        return _log;
                    }
                }
            }
            return _log;
        }
        
        #endregion
    }

}

