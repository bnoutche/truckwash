﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

using TruckWash.Models;

namespace TruckWash.BLL
{
    public class InvoiceBl
    {
        #region Database
        private TruckwashContext _db = new TruckwashContext();
        #endregion

        #region Model
        private Invoice _invoice = new Invoice();
        #endregion

        #region Methods
        public void CreateInvoice(int id, int loginlogid)
        {
            DateTime d = DateTime.Now; 
           
            _invoice.InvoiceNR = CreateInvoiceNumber();
            _invoice.WashTurn = _db.WashTurn.Find(id);
            _invoice.WashTurnID = id;
            _invoice.WashTurn.EndTime = d;
            _invoice.DateInvoice =d;
            VehicleBl vehicle = new VehicleBl();
            Vehicle v = vehicle.GetVehicleById(_invoice.WashTurn.VehicleID);
            v.SecondLastWashturn = v.LastWashturn;
            v.LastWashturn = d;
            vehicle.EditVehicle(v);
            _invoice.WaitTime = WaitTime(_invoice);
            _invoice.LoginLogID = loginlogid;    
            if (_invoice.WashTurn.Vehicle.IsInternVehicle == true)
            {
                _invoice.Paid = true;
            }
            else
            {
                _invoice.Paid = false;
            }

            _invoice.Price = TotalPrice(_invoice);
            _db.Invoice.Add(_invoice);
            _db.SaveChanges();

            SetCustomersInactiveBasedOnInvoicesUnpaid(_invoice);

        }

        public double TotalPrice(Invoice invoice)
        {
              double  price = invoice.WashTurn.WashType.Price + invoice.WashTurn.WashTypeExtra.Price;
              double  discount = (double)invoice.WashTurn.Vehicle.Customer.Discount / 100;
              double  discountPrice = price * discount;
            return price - discountPrice;
        }

        public double WaitTime(Invoice invoice)
        {
             TimeSpan time = invoice.DateInvoice - invoice.WashTurn.WaitTime;
            double sub = time.Days * 24;
            double total =  (time.Hours+sub) *60 +time.Minutes;
            return total;

        }

        public string CreateInvoiceNumber()
        {
            string date = DateTime.Now.ToString("yyMMdd");
            string now = DateTime.Now.ToString("hhmmss");
            string be = "BE";
            return be + date + now;
        }

        public void SetCustomersInactiveBasedOnInvoicesUnpaid(Invoice invoice)
        {

            double totalunpaid =0;
            var list = _db.Invoice.Where(i =>i.Paid == false && i.WashTurn.Vehicle.CustomerID == invoice.WashTurn.Vehicle.CustomerID);


            try
            {
                foreach (Invoice item in list)
                {
                    totalunpaid = +item.Price;
                }

                if (totalunpaid >= 300)
                {

                    Customer cus = _db.Customer.Find(invoice.WashTurn.Vehicle.CustomerID);
                    cus.IsActive = false;
                    _db.Entry(cus).State = EntityState.Modified;
                    _db.SaveChanges();


                }
            }
            catch (Exception)
            {

               
            }

                
        }

        public List<Invoice> GetInvoicesThisMonth()
        {
            var today = DateTime.Today;
            var month = new DateTime(today.Year, today.Month, 1);

            return _db.Invoice.Where(i => i.DateInvoice.Month == month.Month && i.DateInvoice.Year == month.Year).Include(i => i.WashTurn).Include(i => i.LoginLog).ToList();
        }

        public List<Invoice> GetInvoicesLastMonth()
        {
            var today = DateTime.Today;
            var month = new DateTime(today.Year, today.Month,1);
            month=month.AddMonths(-1);
            return _db.Invoice.Where(i => i.DateInvoice.Month == month.Month && i.DateInvoice.Year == month.Year).Include(i => i.WashTurn).Include(i => i.LoginLog).ToList();
        }

        public List<Invoice> GetInvoicesPaid()
        {

            return _db.Invoice.Where(i => i.Paid == true).Include(i => i.WashTurn).Include(i => i.LoginLog).ToList();
        }

        public List<Invoice> GetUnpaidInvoices()
        {

            return _db.Invoice.Where(i => i.Paid == false).Include(i => i.WashTurn).Include(i => i.LoginLog).ToList();
        }

        public List<Invoice> GetOurCompanyVehiclesInvoices()
        {
           
            return _db.Invoice.Where(i => i.WashTurn.Vehicle.IsInternVehicle == true).Include(i => i.WashTurn).Include(i => i.LoginLog).ToList();
        }

        public Invoice GetInvoice(int? id)
        {
            _invoice = _db.Invoice.Find(id);
            return _invoice;
        }

        public Invoice UpdateInvoice(Invoice invoice)
        {
            _db.Entry(invoice).State = EntityState.Modified;
            _db.SaveChanges();
            return invoice;
        }

        public void DeleteInvoice(int id)

        {
             _invoice = GetInvoice(id);
            _db.Invoice.Remove(_invoice);
            _db.SaveChanges();            
        }
       
        public void Dispose()
        {
            _db.Dispose();
        }
        #endregion
    }
}