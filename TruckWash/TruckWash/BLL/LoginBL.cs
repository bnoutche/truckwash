﻿using TruckWash.Models;

namespace TruckWash.BLL
{
    public class LoginBL
    {

        #region Database
        private TruckwashContext db = new TruckwashContext();
        #endregion

        #region Methods
        public bool CheckLoginAsAdmin(LoginLog log)
        {
            if (log.Staff.JobTitel == "Administrator")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool CheckLoginAsOperator(LoginLog log)
        {
            if (log.Staff.JobTitel == "Operator" || log.Staff.JobTitel == "Administrator")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void Dispose()
        {
            db.Dispose();
        }
        #endregion
    }
}