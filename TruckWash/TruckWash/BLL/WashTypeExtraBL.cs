﻿using System.Collections.Generic;
using System.Linq;
using TruckWash.Models;
using System.Data.Entity;

namespace TruckWash.BLL
{
    public class WashTypeExtraBl
    {
        #region Database
        private TruckwashContext _db = new TruckwashContext();
        #endregion

        #region Methods
        public List<WashTypeExtra> GetAllWashTypesExtra()
        {          
            return _db.WashTypeExtra.ToList();
        }

        public WashTypeExtra GetWashTypeExtra(int? id)
        {
            return _db.WashTypeExtra.Find(id);
        }

        public WashTypeExtra CreateWashTypeExtra(WashTypeExtra washTypeExtra)
        {
            _db.WashTypeExtra.Add(washTypeExtra);
            _db.SaveChanges();
            return washTypeExtra;
        }

        public WashTypeExtra UpdateWashTypeExtra(WashTypeExtra washTypeExtra)
        {
            _db.Entry(washTypeExtra).State = EntityState.Modified;
            _db.SaveChanges();
            return washTypeExtra;
        }

        public void DeleteWashTypeExtra(int id)
        {
            WashTypeExtra washTypeExtra = GetWashTypeExtra(id);
            _db.WashTypeExtra.Remove(washTypeExtra);
            _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
        }
        #endregion
    }
}