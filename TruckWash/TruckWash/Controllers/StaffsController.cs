﻿using System.Web.Mvc;
using TruckWash.BLL;
using TruckWash.Models;


namespace TruckWash.Controllers
{
    public class StaffsController : Controller
    {
        #region Controllers
        private LoginController _checkLogin = new LoginController();
        #endregion

        #region BLL
        private StaffBl _staffbl = new StaffBl();
        private QrCodeBl _qrcodebl = new QrCodeBl();
        private LoginBL _loginbl = new LoginBL();
        #endregion

        #region Strings
        private string _sessionName = "Login";
        private string _details = "Details";
        private string _subIndex = "SubIndex";
        private string _error = "Error";
        private string _LoadingPage = "ErrorLoadingPage";
        private string _notFound = "PageNotFound";
        private string _searchList = "SearchList";
        private string _deactive = "De-Activate Now";
        private string _active = "Activate";
        #endregion

        #region Action Methods
        [HttpGet]
        public ActionResult SubIndex()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session[_sessionName] as LoginLog);
        }

        [HttpGet]
        public ActionResult PasswordEditor(int? id)
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session[_sessionName] as LoginLog, _qrcodebl.GetQrCodeById(id.Value));
        }

        [HttpPost]
        public ActionResult PasswordEditor(QrCodes qr)
        {
            //Link from : Staff/details/ ID == staffnumber -> to staff/passwordeditor == id == qrcode (check get)
            //if modelstate is valid -> save password (qrtabel) -> return to staff/details -> int id = staffid (!!!not qrcodeid!!!)
         //   qr.Staff = _staff.GetStaffById(qr.StaffID.Value);
         //   _qrCode.SaveNewPassword(qr);
          

            if (ModelState.IsValid)
            {
                _qrcodebl.UpdateQrCode(qr);
                return RedirectToAction("details", new { id = qr.StaffID.Value });
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session[_sessionName] as LoginLog, _qrcodebl.GetQrCodeById(qr.StaffID.Value));
        }

        [HttpGet]
        public ActionResult List()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session[_sessionName] as LoginLog, _staffbl.GetAllStaffs());
        }

        [HttpGet]
        public ActionResult ActiveSearchList()
        {
            if (Session[_sessionName] != null && _loginbl.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                return View(_searchList, _staffbl.GetAllActiveStaffsWithLocation());
                //return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, _staff.GetAllStaffsWithLocation());
            }
            return _checkLogin.NotLoggedIn();
        }

        [HttpGet]
        public ActionResult DeactiveSearchList()
        {
            if (Session[_sessionName] != null && _loginbl.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                return View(_searchList, _staffbl.GetAllDeactiveStaffsWithLocation());
            }
            return _checkLogin.NotLoggedIn();
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (Session[_sessionName] != null && _loginbl.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {


                ViewBag.Message = "";
                if (id == null)
                {
                    return RedirectToAction(_LoadingPage, _error);
                }
                QrCodes qrcode = _staffbl.GetQrCodeFromStaffId(id.Value);
                //Staff staff = _staff.GetStaffById(id);
                if (qrcode == null)
                {
                    return RedirectToAction(_notFound, _error);
                }

                if (qrcode.Staff.IsActive == true)
                {
                    ViewBag.Message = _deactive;
                }
                else
                {
                    ViewBag.Message = _active;
                }
                return View(qrcode);
            }
            return _checkLogin.NotLoggedIn();
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (Session[_sessionName] != null && _loginbl.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                ViewBag.JobTitel = new SelectList(_staffbl.GetJobtitels());
                return View();
            }
            return _checkLogin.NotLoggedIn();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Staff Staff)
        {
            if (ModelState.IsValid)
            {
                _staffbl.CreateNewStaffMemberWithQrCode(Staff);
                return RedirectToAction(_subIndex);
            }
            else ViewBag.JobTitel = new SelectList(_staffbl.GetJobtitels());
            return _checkLogin.CheckLoggedInAsAdmin(Session[_sessionName] as LoginLog, Staff);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            Staff staff = new Staff();
            if (Session[_sessionName] != null && _loginbl.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                if (id == null)
                {
                    return RedirectToAction(_LoadingPage, _error);
                }
                staff = _staffbl.GetStaffById(id);
                if (staff == null)
                {
                    return HttpNotFound();
                }
                ViewBag.JobTitel = new SelectList(_staffbl.GetJobtitels(), staff.JobTitel);
                return View(staff);
            }
            return _checkLogin.NotLoggedIn();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdStaff,FirstName,LastName,JobTitel,StafNumber,IsActive")] Staff staff)
        {
            if (ModelState.IsValid)
            {
                _staffbl.EditStaffMember(staff);
                return RedirectToAction(_subIndex);
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session[_sessionName] as LoginLog, staff);
        }

        public ActionResult ActiveAndDeActive(bool isTrue, int id)
        {
            if (isTrue)
            {
                _staffbl.ActivateAndDeactivateStaffMember(_staffbl.GetStaffById(id));
                return RedirectToAction(_details, new { id });
            }
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _staffbl.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
