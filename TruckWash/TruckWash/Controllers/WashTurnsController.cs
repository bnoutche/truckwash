﻿using System;
using System.Web.Mvc;
using TruckWash.Models;
using TruckWash.BLL;

namespace TruckWash.Controllers
{
    public class WashTurnsController : Controller
    {

        

        #region BLL
        private WashTurnBl _washTurn = new WashTurnBl();
        private CustomerBl _cus = new CustomerBl();
        private VehicleBl _vehicle = new VehicleBl();
        private WashTypeBl _washType = new WashTypeBl();
       private WashTypeExtraBl _washTypeExtra = new WashTypeExtraBl();
        private LoginBL _login = new LoginBL();
        #endregion

        #region Controllers
        private LoginController _checkLogin = new LoginController();
        #endregion

        #region Strings
        private string _searchList = "SearchList";
        private string _sessionName = "Login";
        #endregion

        #region Action Methods
        [HttpGet]
        public ActionResult SubIndex()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog);
        }

        [HttpGet]
        public ActionResult PlannedWashturns()
        {
            if (Session[_sessionName] != null && _login.CheckLoginAsAdmin(Session[_sessionName]as LoginLog)==true)
            {
                return View(_searchList, _washTurn.GetAllPlannedWashTurns());
            }
            return _checkLogin.NotLoggedIn();
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("ErrorLoadingPage", "Error");
            }
            WashTurn washTurn = _washTurn.GetWashTurn(id);
            if (washTurn == null)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, washTurn);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (Session[_sessionName] != null && _login.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                ViewBag.CustomerID = new SelectList(_cus.GetAllCustomer(), "IdCustomer", "LastName");
                ViewBag.VehicleID = new SelectList(_vehicle.GetAllVehicles(), "IdVehicle", "LicensePlate");
                ViewBag.WashTypeID = new SelectList(_washType.GetAllWashTypes(), "IdWashType", "WashTypeName");
                ViewBag.WashTypeExtraID = new SelectList(_washTypeExtra.GetAllWashTypesExtra(), "IdWashTypeExtra", "WashtypeExtraName");
                return View();
            }

            return _checkLogin.NotLoggedIn();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(WashTurn washTurn)
        {
            if (ModelState.IsValid)
            {
                washTurn.Vehicle = _vehicle.GetVehicleById(washTurn.VehicleID);
                if (washTurn.Vehicle.SecondLastWashturn != null && washTurn.Vehicle.LastWashturn != null)
                {

                    TimeSpan? ts = washTurn.Vehicle.LastWashturn - washTurn.Vehicle.SecondLastWashturn;


                    if (washTurn.Vehicle.Customer.IsActive == false ||
                        ts.Value.Days <= 30 && washTurn.Vehicle.IsInternVehicle)
                    {
                        if (ts.Value.Days <= 30 && washTurn.Vehicle.IsInternVehicle)
                        {
                            return RedirectToAction("ErrorDaysToLess", "Error");
                        }
                        return RedirectToAction("ErrorNotActive", "Error");
                    }
                }

                _washTurn.CreateWashTurn(washTurn);
                return RedirectToAction("SubIndex");
            }

            ViewBag.CustomerID = new SelectList(_cus.GetAllCustomer(), "IdCustomer", "LastName", washTurn.Vehicle.CustomerID);
            ViewBag.VehicleID = new SelectList(_vehicle.GetAllVehicles(), "IdVehicle", "LicensePlate", washTurn.VehicleID);
            ViewBag.WashTypeID = new SelectList(_washType.GetAllWashTypes(), "IdWashType", "WashTypeName", washTurn.WashTypeID);
            ViewBag.WashTypeExtraID = new SelectList(_washTypeExtra.GetAllWashTypesExtra(), "IdWashTypeExtra", "WashtypeExtraName", washTurn.WashTypeExtraID);
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, washTurn);
        }

   
        public ActionResult GetVehiclesByCustomer(int customerid)
        {
            ViewBag.VehicleID = new SelectList(_cus.GetVehicle(customerid), "IdVehicle", "LicensePlate");
            return Json(ViewBag.VehicleID);
        }

        public ActionResult VehicleCheck(int vehicleid)
        {
            Vehicle vehicle = _vehicle.GetVehicleById(vehicleid);
            if (vehicle.IsInternVehicle && vehicle.LastWashturn != null)
            {
                TimeSpan? t = DateTime.Now - vehicle.LastWashturn;
                if (t.Value.Days <= 30)
                {

                    return Json(true);
                }
            }
            return Json(false);
        }

        [HttpGet]
        public ActionResult WashturnNewCustomer()
        {
            if (Session[_sessionName] != null && _login.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                ViewBag.WashTypeID = new SelectList(_washType.GetAllWashTypes(), "IdWashType", "WashTypeName");
                ViewBag.WashTypeExtraID = new SelectList(_washTypeExtra.GetAllWashTypesExtra(), "IdWashTypeExtra", "WashtypeExtraName");
                return View();
            }
            return _checkLogin.NotLoggedIn();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult WashturnNewCustomer(WashTurn washTurn)
        {
            if (ModelState.IsValid)
            {
                _washTurn.CreateWashTurnNewCustomer(washTurn);
                return RedirectToAction("SubIndex", "WashTurns");

            }
            ViewBag.WashTypeID = new SelectList(_washType.GetAllWashTypes(), "IdWashType", "WashTypeName");
            ViewBag.WashTypeExtraID = new SelectList(_washTypeExtra.GetAllWashTypesExtra(), "IdWashTypeExtra", "WashtypeExtraName");
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, washTurn);

        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (Session[_sessionName] != null && _login.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    return RedirectToAction("ErrorLoadingPage", "Error");
                }
                WashTurn washTurn = _washTurn.GetWashTurn(id.Value);
                if (washTurn == null)
                {
                    //return HttpNotFound();
                    return RedirectToAction("PageNotFound", "Error");
                }

                ViewBag.CustomerID = new SelectList(_cus.GetAllCustomer(), "IdCustomer", "LastName", washTurn.Vehicle.CustomerID);
                ViewBag.VehicleID = new SelectList(_vehicle.GetAllVehicles(), "IdVehicle", "LicensePlate", washTurn.VehicleID);
                ViewBag.WashTypeID = new SelectList(_washType.GetAllWashTypes(), "IdWashType", "WashTypeName", washTurn.WashTypeID);
                ViewBag.WashTypeExtraID = new SelectList(_washTypeExtra.GetAllWashTypesExtra(), "IdWashTypeExtra", "WashtypeExtraName", washTurn.WashTypeExtraID);
                return View(washTurn);
            }
            return _checkLogin.NotLoggedIn();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdWashTurn,WaitTime,StartTime,EndTime,Remark,WashTypeID,WashTypeExtraID,CustomerID,VehicleID")] WashTurn washTurn)
        {
            if (ModelState.IsValid)
            {
                _washTurn.UpdateWashTurn(washTurn);
                return RedirectToAction("SubIndex");
            }
            ViewBag.CustomerID = new SelectList(_cus.GetAllCustomer(), "IdCustomer", "LastName", washTurn.Vehicle.CustomerID);
            ViewBag.VehicleID = new SelectList(_vehicle.GetAllVehicles(), "IdVehicle", "LicensePlate", washTurn.VehicleID);
            ViewBag.WashTypeID = new SelectList(_washType.GetAllWashTypes(), "IdWashType", "WashTypeName", washTurn.WashTypeID);
            ViewBag.WashTypeExtraID = new SelectList(_washTypeExtra.GetAllWashTypesExtra(), "IdWashTypeExtra", "WashtypeExtraName", washTurn.WashTypeExtraID);
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, washTurn);
        }

        [HttpGet]
        public ActionResult DeleteConfirmed(int id, bool isTrue)
        {
            if (isTrue)
            {
                _washTurn.DeleteWashTurn(id);
                return RedirectToAction("SubIndex");
            }
            return RedirectToAction("Details", new { id });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _washTurn.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
