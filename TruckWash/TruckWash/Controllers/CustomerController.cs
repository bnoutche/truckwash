﻿using System.Linq;
using System.Web.Mvc;
using TruckWash.BLL;
using TruckWash.Models;

namespace TruckWash.Controllers
{
    public class CustomerController : Controller
    {
        #region BLL
        private CustomerBl _customer = new CustomerBl();
        private AddressBl _address = new AddressBl();
        private  LoginBL _login = new LoginBL();
        #endregion

        #region Controllers
        private LoginController _checkLogin = new LoginController();
       
        #endregion

        #region Strings

        private string _searchList = "SearchList";
        private string _sessionName = "Login";
        #endregion

        #region ActionMethods
        [HttpGet]
        public ActionResult SubIndex()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog);
        }

        [HttpGet]
        public ActionResult GetAllCustomer()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, _customer.GetAllCustomer());
        }

        [HttpGet]
        public ActionResult GetAllCustomerNotActive()
        {

            if (Session[_sessionName] != null && _login.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                return View("GetAllCustomerNotActive", _customer.GetAllNotActiveCustomers());
            }
            return _checkLogin.NotLoggedIn();

        }
        [HttpGet]
        public ActionResult GetAllCustomerActive()
        {
            if (Session[_sessionName] != null && _login.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                return View(_searchList, _customer.GetAllActiveCustomers());
            }
            return _checkLogin.NotLoggedIn();

        }

        [HttpGet]
        public ActionResult CreateCustomer()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCustomer(Customer customer)
        {

            if (ModelState.IsValid)
            {
                _customer.CreateCustomer(customer);
                return RedirectToAction("GetAllCustomerActive");
            }

            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog);

        }
        
        [HttpGet]
        public ActionResult Details(int? id)
        {

            if (Session[_sessionName] != null && _login.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {

                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest); 
                    return RedirectToAction("ErrorLoadingPage", "Error");
                }
                Customer customer = _customer.GetCustomer(id);
                if (customer == null)
                {
                    //return HttpNotFound();
                    return RedirectToAction("PageNotFound", "Error");
                }

                if (customer.IsActive == false)
                {
                    ViewBag.Status = Resources.Customer.DetailsText.StatusActive;
                  
                    ViewBag.StatusName = Resources.Customer.DetailsText.StatusNameDeActive;

                }
                else
                {
                    ViewBag.Status = Resources.Customer.DetailsText.StatusDeActive;
                    ViewBag.StatusName = Resources.Customer.DetailsText.StatusNameActive;
    
                  
                }
                return View(customer);

            }

            return _checkLogin.NotLoggedIn();



        }
        
        public ActionResult CustomerActive(int? id)
        {
            
            if (Session[_sessionName] != null && _login.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {


                if (id != null)
                {
                    Customer customer = _customer.GetCustomer(id);
                    if (customer == null)
                    {
                        //return HttpNotFound();
                        return RedirectToAction("PageNotFound", "Error");
                    }
                    if (customer.IsActive == false)
                    {
                        ViewBag.Status = Resources.Customer.DetailsText.StatusActive;
                        ViewBag.StatusName = Resources.Customer.DetailsText.StatusNameDeActive;

                    }
                    else
                    {
                        ViewBag.Status = Resources.Customer.DetailsText.StatusDeActive;
                    
                        ViewBag.StatusName = Resources.Customer.DetailsText.StatusNameActive;

                    } 

                    return View(customer);
                }
                return RedirectToAction("ErrorLoadingPage", "Error");

            }
            return _checkLogin.NotLoggedIn();

        }

        [HttpGet]
        public ActionResult UpdateCustomerActive(int id, bool isTrue)
        {
         
                    _customer.IsAcive(isTrue, id);
                    return RedirectToAction("CustomerActive", new { id });
            

        }
        
        [HttpGet]
        public ActionResult Update(int? id)
        {

            if (id == null)
            {
                return RedirectToAction("ErrorLoadingPage", "Error");
            }
            Customer customerId = _customer.GetCustomer(id);
            if (customerId == null)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, customerId);
        }

        // POST: Customer/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(Customer updateCustomer)
        {
            if (ModelState.IsValid)
            {
                updateCustomer.Address.IdAddress = updateCustomer.AddressID.Value;
                _address.UpdateAddress(updateCustomer.Address);
                _customer.UpdateCustomer(updateCustomer);
                
                return RedirectToAction("Details", new { id = updateCustomer.IdCustomer });
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, updateCustomer);
        }

        [HttpGet]
        public ActionResult UpdateActive(int id, bool isTrue)
        {
        
                _customer.IsAcive(isTrue, id);
                return RedirectToAction("Details", new { id });
          

           
        }

        [HttpGet]
        public ActionResult GetVehicleList(int? id)
        {

            if (id == null)
            {

                return RedirectToAction("ErrorLoadingPage", "Error");

            }

            var customerVehicles = _customer.GetVehicle(id.Value);
            //return new HttpStatusCodeResult(HttpStatusCode.BadRequest); 
            if (customerVehicles == null)
            {
                //// return HttpNotFound();
                return RedirectToAction("PageNotFound", "Error");
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, customerVehicles.ToList());



        }

        public ActionResult GetInvoices(int? id)
        {
            if (id != null)
            {

                var customerInvoice = _customer.GetInvoices(id);

                if (customerInvoice == null)
                {
                    //// return HttpNotFound();
                    return RedirectToAction("PageNotFound", "Error");
                }
                return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, customerInvoice.ToList());
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest); 

            }
            return RedirectToAction("ErrorLoadingPage", "Error");
        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _customer.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion

    }

}









