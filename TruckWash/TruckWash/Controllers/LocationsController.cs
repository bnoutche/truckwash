﻿using System.Web.Mvc;
using TruckWash.BLL;
using TruckWash.Models;

namespace TruckWash.Controllers
{
    public class LocationsController : Controller
    {
        #region controller
        private LoginController _checkLogin = new LoginController();
        #endregion

        #region BLL
        private AddressBl _addressbl = new AddressBl();
        private LocationBL _locationbl = new LocationBL();
        private LoginBL _loginbl = new LoginBL();
        #endregion

        #region Models
        private Location _location = new Location();
        #endregion

        #region Private members
        private string _sessionName = "Login";
        private string _index = "Index";
        private string _error = "Error";
        private string _notFound = "PageNotFound";
        private string _loadError = "ErrorLoadingPage";

        #endregion

        #region Action Methods
        [HttpGet]
        public ActionResult SubIndex()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session[_sessionName] as LoginLog);
        }

        /// <summary>
        /// GET
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult List()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session[_sessionName] as LoginLog, _locationbl.GetAllLocations());
        }

        /// <summary>
        /// GET
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ActionResult SearchList()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session[_sessionName] as LoginLog, _locationbl.GetAllLocations());
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {            
            if (Session[_sessionName] != null && _loginbl.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                if (id == null)
                {
                    return RedirectToAction(_loadError, _error);
                }
                _location = new Location();
                _location = _locationbl.GetLocationById(id.Value);
                if (_location == null)
                {
                    return RedirectToAction(_notFound, _error);
                }
                return View(_location);
            }
            return _checkLogin.CheckLoggedInAsAdmin(null);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session[_sessionName] as LoginLog);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Location location)
        {
            if (ModelState.IsValid)
            {
                _locationbl.SaveLocation(location);
                return RedirectToAction("SubIndex");
            }
            ViewBag.AddressID = new SelectList(_addressbl.GetAllAddresses(), "IdAddress", "Street", location.AddressID);
            return RedirectToAction("SubIndex");
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction(_loadError, _error);
            }
            _location = new Location();
            _location = _locationbl.GetLocationById(id.Value);
            if (_location == null)
            {
                return RedirectToAction(_notFound, _error);
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog,_location);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Location location)
        {
            if (ModelState.IsValid)
            {
                location.Address.IdAddress = location.AddressID.Value;
                _addressbl.UpdateAddress(location.Address);
                _locationbl.UpdateLocation(location);
                return RedirectToAction("SubIndex");
            }

            return RedirectToAction("SubIndex");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _locationbl.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
