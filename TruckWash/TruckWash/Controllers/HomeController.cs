﻿using System.Web.Mvc;
using TruckWash.BLL;
using TruckWash.Models;

namespace TruckWash.Controllers
{
    public class HomeController : Controller
    {
        #region Controllers
        private LoginController _checkLogin = new LoginController();
        #endregion
        #region Bll
        LoginBL _login = new LoginBL();
        #endregion
        #region Action Methods
        [HttpGet]
        public ActionResult Index()
        {

            var userSession = (Session["Login"] as LoginLog);
            if (userSession != null && _login.CheckLoginAsAdmin((LoginLog)userSession) == true)
            {
                ViewBag.StaffName = userSession.Staff.LastName + " " + userSession.Staff.FirstName;
                ViewBag.StaffNumber = userSession.Staff.StafNumber;
                ViewBag.Lastworklocation = userSession.Location.Address.City;
                ViewBag.Lastworkdate = userSession.Lastworkdate.ToLongDateString();
                ViewBag.StaffId = userSession.StaffID;

                return View();

            }

            return _checkLogin.NotLoggedIn();

        }
        #endregion

    }
}