﻿using System.Web.Mvc;
using Rotativa;
using TruckWash.BLL;
using TruckWash.Models;

namespace TruckWash.Controllers
{
    public class QrCodesController : Controller
    {
        #region Controllers
        private LoginController _loginControl = new LoginController();
        #endregion

        #region Models
        private LoginLog _inlogger = new LoginLog();
        private QrCodes _qrcode = new QrCodes();
        #endregion

        #region BLL
        private Authentication _authenticate = new Authentication();
        private QrCodeBl _qrcodebl = new QrCodeBl();
        private LocationBL _locationbl = new LocationBL();
        private StaffBl _staffbl = new StaffBl();
        private VehicleBl _vehiclebl = new VehicleBl();
        #endregion

        #region Strings
        private string _login = "Login";
        private string _error = "Error";
        private string _notFound = "PageNotFound";
        private string _loadError = "ErrorLoadingPage";
        //voorvoegselQrCode - staat ook in StaffBLL !!! deze moeten dezelfde blijven !
        #endregion

        #region Action Methods
               
        ///Details laad een foto in in de view met een omzetting naar een image van het model -> foto via file png - anders werkt de code niet
        // GET: QrCodes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction(_loadError, _error);
            }
            QrCodes qrCodes = _qrcodebl.GetQrCodeById(id.Value);
            if (qrCodes == null)
            {
                return RedirectToAction(_notFound, _error);
            }
            return _loginControl.CheckLoggedInAsAdmin(Session[_login] as LoginLog, qrCodes);
        }

        public ActionResult QrToPdf(int? id)
        {
            _qrcode = new QrCodes();
            _qrcode = _qrcodebl.GetQrCodeById(id.Value);
            return new ActionAsPdf("QrPdf", new { id = id }) { FileName = "QR.pdf" };
        }

        public ActionResult QrPdf(int? id)
        {
            if (id == null)
            {
                return RedirectToAction(_loadError, _error);
            }
            QrCodes qrCodes = _qrcodebl.GetQrCodeById(id.Value);
            if (qrCodes == null)
            {
                return RedirectToAction(_notFound, _error);
            }
            return View(qrCodes);
        }



        [HttpGet]
        public LoginLog GetSession()
        {
            return (LoginLog)Session[_login];
        }

        [HttpGet]
        public void SetSession(LoginLog PersonThatIsLoggedInNow)
        {
            Session[_login] = PersonThatIsLoggedInNow;
        }

        //[HttpGet]
        //public ActionResult Create()
        //{

        //    ViewBag.StaffID = new SelectList(_staff.GetAllStaffs(), "IdStaff", "FirstName");
        //    ViewBag.VehicleID = new SelectList(_vehicle.GetAllVehicles(), "IdVehicle", "LicensePlate");
        //    return _loginControl.CheckLoggedInAsAdmin(Session["Login"] as LoginLog);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "IdQrCode,StaffID,VehicleID,QrCode")] QrCodes qrCodes)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _qrCode.SaveQrcCode(qrCodes);
        //        return RedirectToAction(_index);
        //    }
        //    ViewBag.StaffID = new SelectList(_staff.GetAllStaffs(), "IdStaff", "FirstName", qrCodes.StaffID);
        //    return View(qrCodes);
        //}

        //public ActionResult Edit(int? id)
        //{
        //    if (Session[_login] as LoginLog != null)
        //    {
        //        if (id == null)
        //        {
        //            // return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //            return RedirectToAction(_loadError, _error);
        //        }
        //        QrCodes qrCodes = _qrCode.GetQrCodesById(id.Value);

        //        if (qrCodes == null)
        //        {
        //            return RedirectToAction(_loadError, _error);
        //        }
        //        if (Session[_login] != null)
        //        {
        //            ViewBag.StaffID = new SelectList(_staff.GetAllStaffs(), "IdStaff", "FirstName", qrCodes.StaffID);
        //            return View(qrCodes);
        //        }

        //    }
        //    return _loginControl.CheckLoggedInAsAdmin(Session[_login] as LoginLog);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "IdQrCode,StaffID,VehicleID,QrCode")] QrCodes qrCodes)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _qrCode.UpdateQrCode(qrCodes);
        //        return RedirectToAction(_index);
        //    }
        //    ViewBag.StaffID = new SelectList(_staff.GetAllStaffs(), "IdStaff", "FirstName", qrCodes.StaffID);
        //    return View(qrCodes);
        //}

        //// GET: QrCodes/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return RedirectToAction(_loadError, _error);
        //    }
        //    QrCodes qrCodes = _qrCode.GetQrCodesById(id.Value);
        //    if (qrCodes == null)
        //    {
        //        return RedirectToAction(_notFound, _error);
        //    }
        //    return _loginControl.CheckLoggedInAsAdmin(Session[_login] as LoginLog, qrCodes);
        //}

        //// POST: QrCodes/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    _qrCode.DeleteQrCode(_qrCode.GetQrCodesById(id));  
        //    return RedirectToAction(_index);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _qrcodebl.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion

    }
}
