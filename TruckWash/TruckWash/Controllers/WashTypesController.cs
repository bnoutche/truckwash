﻿using System.Web.Mvc;
using TruckWash.BLL;
using TruckWash.Models;

namespace TruckWash.Controllers
{
    public class WashTypesController : Controller
    {
        #region BLL
        private WashTypeBl _washType = new WashTypeBl();
        #endregion

        #region Controllers
        private LoginController _checkLogin = new LoginController();
        #endregion

        #region Action Methods
        [HttpGet]
        public ActionResult SubIndex()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, _washType.GetAllWashTypes());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdWashType,WashTypeName,Price")] WashType washType)
        {
            if (ModelState.IsValid)
            {
                _washType.CreateWashType(washType);
                return RedirectToAction("SubIndex");
            }

            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, washType);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("ErrorLoadingPage", "Error");
            }
            WashType washType = _washType.GetWashType(id);
            if (washType == null)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, washType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdWashType,WashTypeName,Price")] WashType washType)
        {
            if (ModelState.IsValid)
            {
                _washType.UpdateWashType(washType);
                return RedirectToAction("SubIndex");
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, washType);
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("ErrorLoadingPage", "Error");
            }
            WashType washType = _washType.GetWashType(id);
            if (washType == null)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, washType);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _washType.DeleteWashType(id);
            return RedirectToAction("SubIndex");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _washType.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
