﻿using System.Web.Mvc;
using TruckWash.BLL;
using TruckWash.Models;

namespace TruckWash.Controllers
{
    public class VehiclesController : Controller
    {
       
        #region Controllers
        private LoginController _checkLogin = new LoginController();       
        #endregion

        #region BLL
        private VehicleBl _vehicleBl = new VehicleBl();
        private CustomerBl _customer = new CustomerBl();
        private LoginBL _login = new LoginBL();
        #endregion

        #region Strings
        private string _sessionName = "Login";
        #endregion

        #region Action Methods
        [HttpGet]
        public ActionResult SubIndex()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog);
        }

        [HttpGet]
        public ActionResult List()
        {

            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, _vehicleBl.GetAllVehicles());

        }

        [HttpGet]
        public ActionResult SearchList()
        {

            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, _vehicleBl.GetAllVehicles());

        }

        [HttpGet]
        public ActionResult OurVehicles()
        {

            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, _vehicleBl.GetAllVehiclesIntern());

        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                return RedirectToAction("ErrorLoadingPage", "Error");

            }
            Vehicle vehicle = _vehicleBl.GetVehicleById(id);
            if (vehicle == null)
            {
                //return HttpNotFound();
                return RedirectToAction("PageNotFound", "Error");
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, vehicle);
        }

        [HttpGet]
        public ActionResult Create()
        {
            
            if (Session[_sessionName] != null && _login.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                ViewBag.CustomerID = new SelectList(_customer.GetAllCustomer(), "IdCustomer", "LastName");
                return View();
            }
            return _checkLogin.NotLoggedIn();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdVehicle,LicensePlate,Merk,Model,Type,Length,Remark,LastWashturn,SecondLastWashturn,IsInternVehicle,CustomerID")] Vehicle vehicle)
        //public ActionResult Create( Vehicle vehicle)
        {
 
                if (ModelState.IsValid)
                {
                    _vehicleBl.CreateNewVehicle(vehicle);
                    //db.Vehicle.Add(vehicle);
                    //db.SaveChanges();
                    return RedirectToAction("SubIndex");
                }

                //ViewBag.CustomerID = new SelectList(db.Customer, "IdCustomer", "LastName", vehicle.CustomerID);
                ViewBag.CustomerID = new SelectList(_customer.GetAllCustomer(), "IdCustomer", "LastName");


                return View(vehicle);


        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                return RedirectToAction("ErrorLoadingPage", "Error");
            }
            Vehicle vehicle = _vehicleBl.GetVehicleById(id);
            if (vehicle == null)
            {
                //return HttpNotFound();
                return RedirectToAction("PageNotFound", "Error");

            }
            if (Session[_sessionName] != null && _login.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                //ViewBag.CustomerID = new SelectList(db.Customer, "IdCustomer", "LastName", vehicle.CustomerID);
                ViewBag.CustomerID = new SelectList(_customer.GetAllCustomer(), "IdCustomer", "LastName");
                return View(vehicle);
            }

            return _checkLogin.NotLoggedIn();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdVehicle,LicensePlate,Merk,Model,Type,Length,Remark,LastWashturn,SecondLastWashturn,IsInternVehicle,CustomerID")] Vehicle vehicle)
        //public ActionResult Edit( Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                _vehicleBl.EditVehicle(vehicle);
                //db.Entry(vehicle).State = EntityState.Modified;
                //db.SaveChanges();
                return RedirectToAction("SubIndex");
            }
            //ViewBag.CustomerID = new SelectList(db.Customer, "IdCustomer", "LastName", vehicle.CustomerID);
            ViewBag.CustomerID = new SelectList(_customer.GetAllCustomer(), "IdCustomer", "LastName");
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, vehicle);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _vehicleBl.Dispose();
                //db.Dispose();
            }
            base.Dispose(disposing);
        }          
        #endregion

    }
}
