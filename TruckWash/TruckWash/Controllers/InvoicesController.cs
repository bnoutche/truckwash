﻿using System.Web.Mvc;
using Rotativa;
using TruckWash.Models;
using TruckWash.BLL;

namespace TruckWash.Controllers
{
    public class InvoicesController : Controller
    {
        #region Controllers
        private LoginController _checkLogin = new LoginController();
        #endregion
        
        #region BLL
        private InvoiceBl _invoice = new InvoiceBl();
        private LoginBL _login = new LoginBL();
        #endregion

        #region Strings
        private string _searchList = "SearchList";
        private string _subIndex = "SubIndex";
        private string _sessionName = "Login";
        #endregion

        #region Action Methods
        [HttpGet]
        public ActionResult SubIndex()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog);
        }

        [HttpGet]
        public ActionResult ThisMonthInvoices()
        {
            
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog,_invoice.GetInvoicesThisMonth());
        }

        [HttpGet]
        public ActionResult LastMonthInvoices()
        {

            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, _invoice.GetInvoicesLastMonth());
        }

        [HttpGet]
        public ActionResult PaidInvoices()
        {

            if (Session[_sessionName] != null && _login.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                return View(_searchList, _invoice.GetInvoicesPaid());
            }
            return _checkLogin.NotLoggedIn();
        }

        [HttpGet]
        public ActionResult UnPaidInvoices()
        {

            if (Session[_sessionName] != null && _login.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                return View(_searchList, _invoice.GetUnpaidInvoices());
            }
            return _checkLogin.NotLoggedIn();
        }

        [HttpGet]
        public ActionResult OurCompanyInvoices()
        {            
            if (Session[_sessionName] != null && _login.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                return View(_searchList, _invoice.GetOurCompanyVehiclesInvoices());
            }
            return _checkLogin.NotLoggedIn();
        }

        [HttpGet]
        public ActionResult CreateInvoiceToDetails(int id)
        {

            if (Session[_sessionName] != null && _login.CheckLoginAsOperator(Session[_sessionName] as LoginLog) == true)
            {
                int loginlogid = (Session["Login"] as LoginLog).IdLoginLog;
                _invoice.CreateInvoice(id, loginlogid);
                return RedirectToAction("SubIndex", "WashHallOperator");
            }

            return _checkLogin.NotLoggedIn();
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            
            if (id == null)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest); 
                return RedirectToAction("ErrorLoadingPage", "Error");
            }
            Invoice invoice = _invoice.GetInvoice(id);
            if (invoice == null)
            {
                //// return HttpNotFound();
                return RedirectToAction("PageNotFound", "Error");
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog,invoice);
        }
        
        [HttpGet]
        public ActionResult PdfDetails(int? id)
        {
            Invoice invoice = _invoice.GetInvoice(id);
            return new ActionAsPdf("PdfPage",new {id=id}) {FileName = "Test.pdf"};
        }

        [HttpGet]
        public ActionResult PdfPage(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("ErrorLoadingPage", "Error");
            }
            Invoice invoice = _invoice.GetInvoice(id);
            if (invoice == null)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            return View(invoice);
        }
        
        [HttpGet]
        public ActionResult Paid(bool isTrue,int id)
        {      
                if (isTrue)
                {
                    Invoice invoice = _invoice.GetInvoice(id);
                    invoice.Paid = true;
                    _invoice.UpdateInvoice(invoice);
                }
                return RedirectToAction("Details", new { id });
       
        }
        
        [HttpGet]
        public ActionResult DeleteConfirmed(int id, bool isTrue)
        {
            if (isTrue == true)
            {
                _invoice.DeleteInvoice(id);
                return RedirectToAction(_subIndex);
            }
                           
            return RedirectToAction("Details", new { id });
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _invoice.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
