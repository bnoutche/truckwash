﻿using System.Web.Mvc;
using TruckWash.Models;

namespace TruckWash.Controllers
{
    public class OurCompanyController : Controller
    {
        #region Controllers
        private LoginController _checkLogin = new LoginController();
        #endregion

        #region Action Methods
        // GET: OurCompany
        public ActionResult SubIndex()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog);
        }
        #endregion

    }
}
