﻿using System.Web.Mvc;
using TruckWash.BLL;
using TruckWash.Models;

namespace TruckWash.Controllers
{
    public class WashTypeExtrasController : Controller
    {
        #region BLL
        private WashTypeExtraBl _washTypeExtra = new WashTypeExtraBl();
        #endregion

        #region Controllers
        private LoginController _checkLogin = new LoginController();
        #endregion

        #region Action Methods
        [HttpGet]
        public ActionResult SubIndex()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, _washTypeExtra.GetAllWashTypesExtra());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdWashTypeExtra,WashtypeExtraName,Price")] WashTypeExtra washTypeExtra)
        {
            if (ModelState.IsValid)
            {
                _washTypeExtra.CreateWashTypeExtra(washTypeExtra);
                return RedirectToAction("SubIndex");
            }

            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, washTypeExtra);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                return RedirectToAction("ErrorLoadingPage", "Error");
            }
            WashTypeExtra washTypeExtra = _washTypeExtra.GetWashTypeExtra(id);
            if (washTypeExtra == null)
            {
                //return HttpNotFound();
                return RedirectToAction("PageNotFound", "Error");

            }
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, washTypeExtra);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdWashTypeExtra,WashtypeExtraName,Price")] WashTypeExtra washTypeExtra)
        {
            if (ModelState.IsValid)
            {
                _washTypeExtra.UpdateWashTypeExtra(washTypeExtra);
                return RedirectToAction("SubIndex");
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, washTypeExtra);
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("ErrorLoadingPage", "Error");
            }
            WashTypeExtra washTypeExtra = _washTypeExtra.GetWashTypeExtra(id);
            if (washTypeExtra == null)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session["Login"] as LoginLog, washTypeExtra);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _washTypeExtra.DeleteWashTypeExtra(id);
            return RedirectToAction("SubIndex");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _washTypeExtra.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
