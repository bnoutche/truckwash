﻿using System.Web.Mvc;
using TruckWash.BLL;
using TruckWash.Models;

namespace TruckWash.Controllers
{
    public class LoginLogsController : Controller
    {
        #region Controllers
        private LoginController _checkLogin = new LoginController();
        #endregion

        #region Models
        private LoginLog _loginlogger = new LoginLog();
        #endregion

        #region BLL
        private LoginLogBL _loginlogbl = new LoginLogBL();
        private LocationBL _locationbl = new LocationBL();
        private StaffBl _staffbl = new StaffBl();
        private  LoginBL _loginbl = new LoginBL();
        #endregion

        #region Strings
        private string _sessionName = "Login";
        private string _searchList = "SearchList";
        private string _index = "Index";
        private string _LoadingPage = "ErrorLoadingPage";
        private string _error = "Error";
        private string _NotFound = "PageNotFound";
        
        #endregion

        #region Action Methods
        [HttpGet]
        public ActionResult SubIndex()
        {
            return _checkLogin.CheckLoggedInAsAdmin(Session[_sessionName] as LoginLog);
        }

        [HttpGet]
        public ActionResult ActiveSearchList()
        {
            if (Session[_sessionName] != null && _loginbl.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                return View(_searchList, _loginlogbl.GetAllActiveLoginlogs());
            }
            return _checkLogin.NotLoggedIn();
        }

        [HttpGet]
        public ActionResult DeactiveSearchList()
        {
            if (Session[_sessionName] != null && _loginbl.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                return View(_searchList, _loginlogbl.GetAllDeactivatedLoginlogs());
            }
            return _checkLogin.NotLoggedIn();
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            { 
                return RedirectToAction(_LoadingPage, _error);
            }       
                _loginlogger = _loginlogbl.GetLoginlogById(id.Value);
            

            if (_loginlogger == null)
            {
                return RedirectToAction(_NotFound, _error);
            }
            return _checkLogin.CheckLoggedInAsAdmin(Session[_sessionName] as LoginLog,_loginlogger);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (Session[_sessionName] != null && _loginbl.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                ViewBag.LocationID = new SelectList(_locationbl.GetAllLocations(), "IdLocation", "CustomerID");
                ViewBag.StaffID = new SelectList(_staffbl.GetAllStaffs(), "IdStaff", "FirstName");
                return View();
            }
            return _checkLogin.NotLoggedIn();
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdLoginLog,Lastworkdate,Lastworklocation,LocationID,StaffID")] LoginLog loginLog)
        {
                if (ModelState.IsValid)
                {
                    _loginlogbl.SaveLoginlog(loginLog);
                    return RedirectToAction(_index);
                }
                ViewBag.LocationID = new SelectList(_locationbl.GetAllLocations(), "IdLocation", "CustomerID", loginLog.LocationID);
                ViewBag.StaffID = new SelectList(_staffbl.GetAllStaffs(), "IdStaff", "FirstName", loginLog.StaffID);
                return View(loginLog);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (Session[_sessionName] != null && _loginbl.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                if (id == null)
                {
                    return RedirectToAction(_LoadingPage, _error);
                }
                _loginlogger = _loginlogbl.GetLoginlogById(id.Value);
                if (_loginlogger == null)
                {
                    return RedirectToAction(_NotFound, _error);
                }
                ViewBag.LocationID = new SelectList(_locationbl.GetAllLocations(), "IdLocation", "Address",
                    _loginlogger.LocationID);
                ViewBag.StaffID = new SelectList(_staffbl.GetAllStaffs(), "IdStaff", "FirstName", _loginlogger.StaffID);
                return View(_loginlogger);
            }
            return _checkLogin.NotLoggedIn();
        }
                
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdLoginLog,Lastworkdate,Lastworklocation,LocationID,StaffID")] LoginLog loginLog)
        {
                if (ModelState.IsValid)
                {
                    _loginlogbl.UpdateLoginlog(loginLog);
                    return RedirectToAction(_index);
                }
                ViewBag.LocationID = new SelectList(_locationbl.GetAllLocations(), "IdLocation", "Address", loginLog.LocationID);
                ViewBag.StaffID = new SelectList(_staffbl.GetAllStaffs(), "IdStaff", "FirstName", loginLog.StaffID);
                return View(loginLog);
        }
        
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (Session[_sessionName] != null && _loginbl.CheckLoginAsAdmin(Session[_sessionName] as LoginLog) == true)
            {
                if (id == null)
                {
                    return RedirectToAction(_LoadingPage, _error);
                }
                _loginlogger = _loginlogbl.GetLoginlogById(id.Value);
                if (_loginlogger == null)
                {
                    return RedirectToAction(_NotFound, _error);
                }
                return View(_loginlogger);
            }
            return _checkLogin.NotLoggedIn();
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _loginlogbl.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
