﻿using System;
using System.Web.Mvc;
using TruckWash.Models;
using TruckWash.BLL;

namespace TruckWash.Controllers
{
    public class WashHallOperatorController : Controller
    {
        #region BLL
        private WashTurnBl _washTurn = new WashTurnBl();
        private LoginBL _login = new LoginBL();
        #endregion

        #region Strings
        private string _List = "List";
        private string _sessionName = "Login";
        #endregion

        #region Controllers
        LoginController _checkLogin = new LoginController();
        #endregion

        #region Action Methods
        [HttpGet]
        public ActionResult SubIndex()
        {
            var userSession = (Session["Login"] as LoginLog);
            if (Session[_sessionName] != null && _login.CheckLoginAsOperator(Session[_sessionName] as LoginLog) == true)
            {
                ViewBag.StaffName = userSession.Staff.LastName + " " + userSession.Staff.FirstName;
                ViewBag.StaffNumber = userSession.Staff.StafNumber;
                ViewBag.Lastworklocation = userSession.Location.Address.City;
                ViewBag.Lastworkdate = userSession.Lastworkdate.ToLongDateString();
                ViewBag.StaffId = userSession.StaffID;
                return View();
            }
            return _checkLogin.NotLoggedIn();
        }

        [HttpGet]
        public ActionResult PlannedWashTurns()
        {

            if (Session[_sessionName] != null && _login.CheckLoginAsOperator(Session[_sessionName] as LoginLog) == true)
            {
                return View(_List, _washTurn.GetAllPlannedWashTurns());
            }
            return _checkLogin.NotLoggedIn();
        }

        public ActionResult StartedWashTurns()
        {

            if (Session[_sessionName] != null && _login.CheckLoginAsOperator(Session[_sessionName] as LoginLog) == true)
            {
                return View(_List, _washTurn.GetAllStartedWashTurns());
            }
            return _checkLogin.NotLoggedIn();
        }
   
        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                return RedirectToAction("ErrorLoadingPage", "Error");
            }
            WashTurn washTurn = _washTurn.GetWashTurn(id);
            
      
            if (washTurn == null)
            {
                //return HttpNotFound();
                return RedirectToAction("PageNotFound", "Error");
            }
            return _checkLogin.CheckLoggedInAsOperator(Session["Login"] as LoginLog, washTurn);
        }

        public PartialViewResult StartTime(int? id)
        {
            WashTurn model = _washTurn.GetWashTurn(id);
            model.StartTime = DateTime.Now;
            _washTurn.UpdateWashTurn(model);

            return PartialView("_Details", model);
        }
        #endregion

    }
}