﻿using System.Web.Mvc;
using TruckWash.BLL;
using TruckWash.Models;



namespace TruckWash.Controllers
{
    public class LoginController : Controller
    {
        #region BLL
        private Authentication _authentication = new Authentication();
        private LoginBL _login = new LoginBL();
        private StaffBl _staff = new StaffBl();
        private LocationBL _location = new LocationBL();
        private QrCodeBl _qrCode = new QrCodeBl();
        #endregion

        #region Model
        private LoginLog _inlogger = new LoginLog();
        #endregion

        #region Controllers

        private LoginLog _loginLogger = new LoginLog();
        #endregion

        #region Strings
        private string _defRoute = "Login";
        private string _defController = "Welcome";
        private string _administrator = "Administrator";
        private string _operator = "Operator";
        private string _sessionName = "Login";
        private string _index = "Index";
        private string _home = "Home";
        private string _washHallOperator = "WashHallOperator";
        private string _subIndex = "SubIndex";
        #endregion

        #region Action Methods
        [HttpGet]
        public ActionResult LogOut()
        {
            if (Session[_sessionName] != null)
            {
                Session[_sessionName] = null;

                return RedirectToAction(_defController, _defRoute);
            }
            return RedirectToAction(_index, _home);
        }

        [HttpGet]
        public ActionResult Welcome()
        {
            _loginLogger = GetSession();
            if (_loginLogger != null && _staff.GetAllStaffs() != null)
            {
                return CheckRightsAndReturnPage(_staff.GetStaffById(_loginLogger.StaffID));
            }
            return View();
        }


        [HttpGet]
        public ActionResult CheckRightsAndReturnPage(Staff staffmember)
        {
            if (staffmember.JobTitel == _administrator)
            {
                return RedirectToAction(_index, _home);
            }
            else if (staffmember.JobTitel == _operator)
            {
                return RedirectToAction(_subIndex, _washHallOperator);
            }
            return View(_defController, _defRoute);
        }
        [HttpGet]

        public ActionResult SendToHomePage()
        {
            return RedirectToAction(_defController, _defRoute);
        }

        [HttpGet]
        public ActionResult Login()
        {
            _loginLogger = GetSession();
            if (_loginLogger != null && _staff.GetAllStaffs() != null)
            {
                return CheckRightsAndReturnPage(_staff.GetStaffById(_loginLogger.StaffID));
            }
            ViewBag.LocationID = new SelectList(_location.GetAllLocations(), "IdLocation", "LocationName");
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginLog inlogdata)
        {
            //staff lastname is de opvang van het paswoord
            _loginLogger = new LoginLog();
            //bool is nodig voor de Login controle
            bool isQrCode = false;
            _loginLogger = _authentication.InloggenFromLoginPage(inlogdata.Staff.FirstName, inlogdata.Staff.LastName, inlogdata.LocationID, isQrCode);            
            //firstname = username       Lastname == paswoord       locationid = locationid

            //staff voor de sessie
            Staff loggedStaff = _staff.GetStaffById(_loginLogger.StaffID);
            //staf is actief check
            if (loggedStaff != null && loggedStaff.IsActive == false)
            {
                return RedirectToAction("ErrorStaffNotActive", "Error");
            }

            // voorwaarde if not 0 anders is er een sessie met de tijd en is bij de sessie != null 
            if (_loginLogger.LocationID != 0 && _loginLogger.IdLoginLog != 0)
            {
                SetSession(_loginLogger);
                return CheckRightsAndReturnPage(_loginLogger.Staff);
            }
            else
            {
                ViewBag.LocationID = new SelectList(_location.GetAllLocations(), "IdLocation", "LocationName");
                ViewBag.LoginFault = "loginfout";
                return View();
            }
        }

        [HttpGet]
        public ActionResult CheckLoggedInAsOperator(LoginLog loginlogger)
        {
            if (loginlogger == null)
            {
                return SendBackToLoginView();
            }
            return View();
        }

        [HttpGet]
        public ActionResult CheckLoggedInAsOperator(LoginLog loginlogger, object obj)
        {
            if (loginlogger == null)
            {
                return SendBackToLoginView();
            }
            return View(obj);
        }

        [HttpGet]
        public ActionResult CheckLoggedInAsAdmin(LoginLog loginlogger)
        {
            if (loginlogger == null || loginlogger.Staff.JobTitel != _administrator)
            {
                return SendBackToLoginView();
            }
            return View();
        }

        [HttpGet]
        public ActionResult CheckLoggedInAsAdmin(LoginLog loginlogger, object obj)
        {
            if (loginlogger == null || loginlogger.Staff.JobTitel != _administrator)
            {
                return SendBackToLoginView();
            }
            return View(obj);
        }

        [HttpGet]
        public ActionResult NotLoggedIn()
        {
            return RedirectToAction(_defController, _defRoute);
        }

        [HttpGet]
        public ActionResult SendBackToLoginView()
        {
            return RedirectToAction(_defController, _defRoute);
        }

        public LoginLog GetSession()
        {
            return _loginLogger = (LoginLog)Session[_sessionName];
        }

        [HttpGet]
        public ActionResult camreader()
        {
            //        string getdata = (string)Session["temp"];
            //       byte[] aa = (byte[])Session["temp"];
            if (Session["Login"] != null)
            {
                return CheckRightsAndReturnPage((Session["Login"] as LoginLog).Staff);
            }
            ViewBag.Location = _location.GetAllLocations();
            return View();
        }

        public ActionResult camreader(string[] myName)
        {
            bool passed = false;
            int locatieID = int.Parse(myName[0]);
            int teller = 0;

            foreach (var qrFotoString in myName)
            {

                if (teller!= 0)
                {
                   //de qrFotoString komt van het javascript camread. meerdere fotos in een aaray        
                    string scannedPhotoAsString = _qrCode.GetScannedQrCodeStringText(qrFotoString);

                    if (scannedPhotoAsString != null)
                    {
                        //scannedPhotoAsString -> is een encrypted password. hieronder wordt de decryptie gedaan
                        string QrCodeUitgelezenPasswoord = _qrCode.GetQrPassword(scannedPhotoAsString);

                        var AllCodes = _qrCode.GetAllQrCodes();

                        foreach (QrCodes item in AllCodes)
                        {
                            // string itemQrPaswoordfromdb = item.Staff.StafNumber.ToString();
                            //als de gedecypteerde decoded qrcode == aan de staffnummer ==> login
                            if (QrCodeUitgelezenPasswoord == item.Staff.StafNumber.ToString())
                            {
                                bool isQrCode = true;
                                string username = item.Staff.FirstName + item.Staff.StafNumber;
                                _inlogger = _authentication.InloggenFromLoginPage(username, item.Password, locatieID, isQrCode);
                                SetSession(_inlogger);
                                passed = true;
                            }                            
                        }
                    }
                }
                teller++;
            }
            if (passed)
            {
                return View("Welcome");
            }
            return camreader();
        }


        public void SetSession(LoginLog PersonThatIsLoggedInNow)
        {
            Session[_sessionName] = Session.IsCookieless;
            Session[_sessionName] = PersonThatIsLoggedInNow;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _login.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
