﻿using System.Web.Mvc;

namespace TruckWash.Controllers
{
    public class ErrorController : Controller
    {
        #region Action Methods
        [HttpGet]
        public ActionResult PageNotFound()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ErrorLoadingPage()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ErrorNotActive()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ErrorDaysToLess()
        {
            return View();
        }
        public ActionResult ErrorStaffNotActive()
        {
            return View();
        }
        #endregion

    }
}