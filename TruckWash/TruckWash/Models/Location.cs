namespace TruckWash.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Location")]
    public partial class Location
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Location()
        {
            LoginLog = new HashSet<LoginLog>();
        }

        [Key]
        public int IdLocation { get; set; }

        public string LocationName { get; set; }

        public string CompanyName { get; set; }

        public string BtwNumber { get; set; }

        [Required]
        public string Email { get; set; }

        public string Phone { get; set; }

        public int? AddressID { get; set; }

        //public bool IsActive { get; set; }

        public virtual Address Address { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LoginLog> LoginLog { get; set; }
    }
}
