namespace TruckWash.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Vehicle")]
    public partial class Vehicle
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Vehicle()
        {
            WashTurn = new HashSet<WashTurn>();
        }

        [Key]
        public int IdVehicle { get; set; }

        [Required]
        [StringLength(25)]
        public string LicensePlate { get; set; }

        [Required]
        [StringLength(120)]
        public string Merk { get; set; }

        [Required]
        [StringLength(150)]
        public string Model { get; set; }

        public string Type { get; set; }

        public decimal? Length { get; set; }

        public string Remark { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LastWashturn { get; set; }

        [Column(TypeName = "date")]
        public DateTime? SecondLastWashturn { get; set; }

        public bool IsInternVehicle { get; set; }

        public int CustomerID { get; set; }

        public virtual Customer Customer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WashTurn> WashTurn { get; set; }
    }
}
