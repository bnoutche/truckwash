namespace TruckWash.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WashTypeExtra")]
    public partial class WashTypeExtra
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WashTypeExtra()
        {
            WashTurn = new HashSet<WashTurn>();
        }

        [Key]
        public int IdWashTypeExtra { get; set; }

        [Required]
        public string WashtypeExtraName { get; set; }

        public double Price { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WashTurn> WashTurn { get; set; }
    }
}
