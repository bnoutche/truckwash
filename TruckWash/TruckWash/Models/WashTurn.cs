namespace TruckWash.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WashTurn")]
    public partial class WashTurn
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WashTurn()
        {
            Invoice = new HashSet<Invoice>();
        }

        [Key]
        public int IdWashTurn { get; set; }

        public DateTime WaitTime { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public string Remark { get; set; }

        public int WashTypeID { get; set; }

        public int? WashTypeExtraID { get; set; }

        public int VehicleID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice> Invoice { get; set; }

        public virtual Vehicle Vehicle { get; set; }

        public virtual WashType WashType { get; set; }

        public virtual WashTypeExtra WashTypeExtra { get; set; }
    }
}
