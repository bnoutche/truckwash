namespace TruckWash.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TruckwashContext : DbContext
    {
        public TruckwashContext()
            : base("name=TruckWashEntities")
        {
        }

        public virtual DbSet<Address> Address { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Invoice> Invoice { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<LoginLog> LoginLog { get; set; }
        public virtual DbSet<QrCodes> QrCodes { get; set; }
        public virtual DbSet<Staff> Staff { get; set; }
        public virtual DbSet<Vehicle> Vehicle { get; set; }
        public virtual DbSet<WashTurn> WashTurn { get; set; }
        public virtual DbSet<WashType> WashType { get; set; }
        public virtual DbSet<WashTypeExtra> WashTypeExtra { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>()
                .HasMany(e => e.Customer)
                .WithOptional(e => e.Address)
                .HasForeignKey(e => e.AddressID);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.Location)
                .WithOptional(e => e.Address)
                .HasForeignKey(e => e.AddressID);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.Vehicle)
                .WithRequired(e => e.Customer)
                .HasForeignKey(e => e.CustomerID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Location>()
                .HasMany(e => e.LoginLog)
                .WithRequired(e => e.Location)
                .HasForeignKey(e => e.LocationID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LoginLog>()
                .HasMany(e => e.Invoice)
                .WithRequired(e => e.LoginLog)
                .HasForeignKey(e => e.LoginLogID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Staff>()
                .HasMany(e => e.LoginLog)
                .WithRequired(e => e.Staff)
                .HasForeignKey(e => e.StaffID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Staff>()
                .HasMany(e => e.QrCodes)
                .WithOptional(e => e.Staff)
                .HasForeignKey(e => e.StaffID);

            modelBuilder.Entity<Vehicle>()
                .Property(e => e.Length)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.WashTurn)
                .WithRequired(e => e.Vehicle)
                .HasForeignKey(e => e.VehicleID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WashTurn>()
                .HasMany(e => e.Invoice)
                .WithRequired(e => e.WashTurn)
                .HasForeignKey(e => e.WashTurnID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WashType>()
                .HasMany(e => e.WashTurn)
                .WithRequired(e => e.WashType)
                .HasForeignKey(e => e.WashTypeID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WashTypeExtra>()
                .HasMany(e => e.WashTurn)
                .WithOptional(e => e.WashTypeExtra)
                .HasForeignKey(e => e.WashTypeExtraID);
        }
    }
}
