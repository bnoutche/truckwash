namespace TruckWash.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class QrCodes
    {
        [Key]
        public int IdQrCode { get; set; }

        [Required]
        public byte[] QrCode { get; set; }

        [Required]
        public string Password { get; set; }

        public int? StaffID { get; set; }

        public virtual Staff Staff { get; set; }
    }
}
