namespace TruckWash.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LoginLog")]
    public partial class LoginLog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LoginLog()
        {
            Invoice = new HashSet<Invoice>();
        }

        [Key]
        public int IdLoginLog { get; set; }

        public DateTime Lastworkdate { get; set; }

        public int LocationID { get; set; }

        public int StaffID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice> Invoice { get; set; }

        public virtual Location Location { get; set; }

        public virtual Staff Staff { get; set; }
    }
}
