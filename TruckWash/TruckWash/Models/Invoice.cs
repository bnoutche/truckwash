namespace TruckWash.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Invoice")]
    public partial class Invoice
    {
        [Key]
        public int IdInvoice { get; set; }

        [Required]
        public string InvoiceNR { get; set; }

        public double Price { get; set; }

        public double WaitTime { get; set; }

        public bool Paid { get; set; }

        public DateTime DateInvoice { get; set; }

        public int LoginLogID { get; set; }

        public int WashTurnID { get; set; }

        public virtual LoginLog LoginLog { get; set; }

        public virtual WashTurn WashTurn { get; set; }
    }
}
