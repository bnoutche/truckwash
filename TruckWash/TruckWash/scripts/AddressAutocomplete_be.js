/* global
    jQuery,
    pro6pp_auth_key */

// Create closure to keep namespace clean and hide implementation.
(function($) {
  'use strict';

  $.fn.applyAutocompleteBE = function(options) {
    this.apiUrl = 'https://api.pro6pp.nl/v1';
    /*jshint camelcase: false */
    this.pro6ppAuthKey = (options && options.pro6ppAuthKey) || pro6pp_auth_key;
    /*jshint camelcase: true */

    var self = this;

    this.postcode = this.find('input[data-binding=postcode]');
    this.postcodeValidated = this.find('*[data-binding="postcode-validated"]');
    this.street = this.find('input[data-binding=street]');
    this.streetValidated = this.find('*[data-binding="street-validated"]');
    this.streetnumber = this.find('input[data-binding=streetnumber]');
    this.streetnumberValidated = this.find(
      '*[data-binding="streetnumber-validated"]');
    this.streetNL = this.find('input[data-binding=street-nl]');
    this.streetFR = this.find('input[data-binding=street-fr]');
    this.cityNL = this.find('input[data-binding=city-nl]');
    this.cityFR = this.find('input[data-binding=city-fr]');
    this.municipalityNL = this.find('input[data-binding=municipality-nl]');
    this.municipalityFR = this.find('input[data-binding=municipality-fr]');
    this.provinceNL = this.find('input[data-binding=province-nl]');
    this.provinceFR = this.find('input[data-binding=province-fr]');
    this.lat = this.find('input[data-binding=lat]');
    this.lng = this.find('input[data-binding=lng]');

    function invalidatePostcode() {
      if (self.postcode.data('geodata') &&
          self.postcode.val() !== self.postcode.data('geodata').label) {
        self.postcode.removeData('geodata');
        self.postcodeValidated.css('display', 'none');
      }
    }

    this.postcode.autocomplete({
      source: function( request, response ) {
        /*jshint camelcase: false */
        var data = {
          auth_key: self.pro6ppAuthKey,
          per_page: 10
        };
        /*jshint camelcase: true */
        // Determine whether to ask for city or postcode
        if (/^[0-9]/.test(request.term)) {
          /*jshint camelcase: false */
          data.be_fourpp = request.term;
          /*jshint camelcase: true */
        } else {
          /*jshint camelcase: false */
          data.be_city = request.term;
          /*jshint camelcase: true */
        }
        $.ajax({
          url: self.apiUrl + '/autocomplete',
          dataType: "jsonp",
          data: data,
          success: function( res ) {
            if (res.results.length) {
              var d = [];
              res.results.forEach(function(result) {
                /*jshint camelcase: false */
                var sl = !!data.be_city && data.be_city.toLowerCase();
                if ((!sl) || (result.municipality_nl.toLowerCase().lastIndexOf(
                        sl, 0) === 0) || (
                    result.city_nl.toLowerCase().lastIndexOf(sl, 0) === 0)) {
                  result.label = result.city_nl + ', ' +
                    result.municipality_nl + ', ' +
                    result.fourpp;
                  /*jshint camelcase: true */

                  var entryNL = {
                    value: result.fourpp,
                    label: result.label,
                    data: result};
                  d.push(entryNL);
                }
                /*jshint camelcase: false */
                if ((!sl) || (result.municipality_fr.toLowerCase().lastIndexOf(
                        sl, 0) === 0) || (
                    result.city_fr.toLowerCase().lastIndexOf(sl, 0) === 0)) {
                  result.label = result.city_fr + ', ' +
                    result.municipality_fr + ', ' +
                    result.fourpp;
                  /*jshint camelcase: true */

                  var entryFR = {
                    value: result.fourpp,
                    label: result.label,
                    data: result};
                  d.push(entryFR);
                }
              });
              response(d);
            } else {
              invalidatePostcode();
              response();
            }
          },
          error: function( jqXHR, textStatus, errorThrown) {
            invalidatePostcode();
          }
        });
      },
      minLength: 1,
      select: function( event, ui ) {
        if (ui.item) {
          self.postcode.val( ui.item.label );
          self.postcode.data('geodata', ui.item.data);
          self.postcodeValidated.css('display', 'inline-block');
          /*jshint camelcase: false */
          self.cityNL.val(ui.item.data.city_nl);
          self.cityFR.val(ui.item.data.city_fr);
          self.municipalityNL.val(ui.item.data.municipality_nl);
          self.municipalityFR.val(ui.item.data.municipality_fr);
          self.provinceNL.val(ui.item.data.province_nl);
          self.provinceFR.val(ui.item.data.province_fr);
          /*jshint camelcase: true */
          self.lat.val(ui.item.data.lat);
          self.lng.val(ui.item.data.lng);

          return false;
        }
      }
    }).change(function() {
      invalidatePostcode();
    });

    function invalidateStreet() {
      if (self.street.val() !== self.street.data('geodata').label) {
        self.street.removeData('geodata');
        self.streetValidated.css('display', 'none');
      }
    }

    this.street.autocomplete({
      source: function( request, response ) {
        if (!self.postcode.data('geodata')) {
          console.log('First select a municipality');
          return;
        }
        $.ajax({
          url: self.apiUrl + '/autocomplete',
          dataType: "jsonp",
          data: {
            /*jshint camelcase: false */
            auth_key: pro6pp_auth_key,
            per_page: 10,
            be_fourpp: self.postcode.data('geodata').fourpp,
            /*jshint camelcase: true */
            street: request.term
          },
          success: function( res ) {
            if (res.results.length) {
              var d = [];
              res.results.forEach(function(result) {
                /*jshint camelcase: false */
                var sl = request.term.toLowerCase();
                if (result.street_nl &&
                    (result.street_nl.toLowerCase().lastIndexOf(
                        sl, 0) === 0)) {
                  result.label = result.street_nl;
                  var entryNL = {
                    value: result.street_nl,
                    label: result.label,
                    data: result};
                  d.push(entryNL);
                }
                if (result.street_fr &&
                    (result.street_fr.toLowerCase().lastIndexOf(
                        sl, 0) === 0)) {
                  result.label = result.street_fr;
                  var entryFR = {
                    value: result.street_fr,
                    /*jshint camelcase: true */
                    label: result.label,
                    data: result};
                  d.push(entryFR);
                }
              });
              response(d);
            } else {
              invalidateStreet();
              response();
            }
          },
          error: function( jqXHR, textStatus, errorThrown) {
            console.log('nothing');
          }
        });
      },
      minLength: 1,
      select: function( event, ui ) {
        if (ui.item) {
          self.street.val( ui.item.label );
          self.street.data('geodata', ui.item.data);
          self.streetValidated.css('display', 'inline-block');
          /*jshint camelcase: false */
          self.streetNL.val(ui.item.data.street_nl);
          self.streetFR.val(ui.item.data.street_fr);
          /*jshint camelcase: true */
          self.lat.val(ui.item.data.lat);
          self.lng.val(ui.item.data.lng);
          return false;
        }
      }
    }).change(function() {
      invalidateStreet();
    });

    function invalidateStreetnumber() {
      if (self.streetnumber.val() !== self.streetnumber.data('geodata')) {
        self.streetnumber.removeData('geodata');
        self.streetnumberValidated.css('display', 'none');
      }
    }

    this.streetnumber.autocomplete({
      source: function( request, response ) {
        if (!self.street.data('geodata')) {
          console.log('First select a street');
          return;
        }

        $.ajax({
          url: self.apiUrl + '/autocomplete',
          dataType: "jsonp",
          data: {
            /*jshint camelcase: false */
            auth_key: pro6pp_auth_key,
            be_fourpp: self.postcode.data('geodata').fourpp,
            street: self.street.data('geodata').street_nl ||
                    self.street.data('geodata').street_fr,
            /*jshint camelcase: true */
            streetnumber: request.term
          },
          success: function( data ) {
            if (data.results.length === undefined) {
              var entry = {
                value: data.results.streetnumber,
                label:data.results.streetnumber,
                data: data.results};
              response([entry]);
            } else {
              invalidateStreetnumber();
              response();
            }
          },
          error: function( jqXHR, textStatus, errorThrown) {
            console.log('nothing');
          }
        });
      },
      minLength: 1,
      select: function( event, ui ) {
        if (ui.item) {
          self.streetnumber.val( ui.item.label );
          self.streetnumber.data('geodata', ui.item.value);
          self.streetnumberValidated.css('display', 'inline-block');
          self.lat.val(ui.item.data.lat);
          self.lng.val(ui.item.data.lng);
          return false;
        }
      }
    }).change(function() {
      invalidateStreetnumber();
    });
  };

  //
  // end of closure
  //
})(jQuery);
