﻿window.addEventListener("load", completePostalCodes, false);


function completePostalCodes() {

    $("#postalCode").keyup(function () {
        var postalCodes = $(this).val();


        if ((postalCodes.length === 4)) {

            // Make HTTP Request
            $.ajax({
                url: "http://api.zippopotam.us/BE/" + postalCodes,
                cache: false,
                dataType: "json",
                type: "GET",
                success: function (result, success) {

                    // US Zip Code Records Officially Map to only 1 Primary Location
                    places = result['places'][0];
                    $("#cityName").val(places['place name']);
                    $("#Province").val(places['state']);

                },
                error: function (result, success) {
                    console.log(success);
                }
            });
        }

    });
}
