﻿
    function GetVehicles(_customerId) {
        var procemessage = "<option selected disabled> Please wait...</option>";
        $("#VehicleID").html(procemessage).show();
        var url = "../Washturns/GetVehiclesByCustomer/";

        $.ajax({
            url: url,
            data: { CustomerID: _customerId },
            cache: false,
            type: "POST",
            success: function(data) {
                var markup = "<option selected disabled hidden style ='display:none'>Select Vehicle</option>";
                for (var x = 0; x < data.length; x++) {
                    markup += "<option value=" + data[x].Value + ">" + data[x].Text + "</option>";
                }
                $("#VehicleID").html(markup).show();
            },
            error: function(reponse) {
                alert("error : " + reponse);
            }
        });

    }


